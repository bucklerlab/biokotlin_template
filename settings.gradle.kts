rootProject.name = "biokotlin_template"

sourceControl {
    gitRepository(java.net.URI.create("https://bitbucket.org/bucklerlab/biokotlin.git")) {
        producesModule("org.biokotlin:biokotlin")
    }
}
