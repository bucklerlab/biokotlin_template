import biokotlin.seq.NucSeqRecord
import biokotlin.seqIO.SeqIO
import kotlin.system.measureNanoTime

println()
val seqio = SeqIO("src/test/resources/B73_Ref_Subset.fa")
val time = measureNanoTime {
    seqio.forEachIndexed { index, record ->
        println("$index: ${(record as NucSeqRecord).id}: ${record.sequence.size()}")
    }
}
println("time: ${time / 1e9} secs.")