**This repository shows example how to use BioKotlin (https://www.biokotlin.org) library with your Kotlin scripts and how to execute them using gradle**

---

## Getting Started

1. git clone https://bitbucket.org/bucklerlab/biokotlin_template.git
2. Remove the biokotlin_template/.git directory and rename biokotlin_template, as you'll probably want to start a new repository. Or you can use these files in your existing repository.
3. In IntelliJ, File -> New -> Project from Existing Sources...
4. Navigate to your newly created directory
5. Edit build.gradle.kts to specify the branch of BioKotlin that you are using if its not "develop".

---

## Executing on CBSU

1. Install Kotlinc (https://github.com/JetBrains/kotlin/releases/tag/v1.4.10) if not already installed. (i.e. wget https://github.com/JetBrains/kotlin/releases/download/v1.4.10/kotlin-compiler-1.4.10.zip)
2. Install Gradle (https://gradle.org/releases) (i.e. wget https://services.gradle.org/distributions/gradle-6.6.1-bin.zip)
3. Add kotlinc to your PATH. (i.e. export PATH=/workdir/<userid>/kotlinc/bin:/workdir/<userid>/gradle-6.6.1/bin:$PATH)
4. Java version 11 or higher is required (i.e. java -version)
5. Test example "gradle BioKotlinExample"
6. Edit build.gradle.kts and create task similar to task(BioKotlinExample) to point to your script
7. To execute your script "gradle <script name>"
8. To increase heap "export JAVA_OPTS="-Xmx8g""