val kotlinVersion = "1.4.10"

repositories {
    mavenCentral()
    jcenter()
    maven("https://maven.imagej.net/content/groups/public/")
    maven("https://jitpack.io")
    maven("https://dl.bintray.com/kotlin/kotlin-eap")
    maven("https://kotlin.bintray.com/kotlinx")
    maven("https://oss.sonatype.org/content/repositories/snapshots/")
    maven("http://www.ebi.ac.uk/~maven/m2repo")  // Uniprot JAPI
}

plugins {
    val kotlinVersion = "1.4.10"
    java
    kotlin("jvm") version kotlinVersion
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

dependencies {
    implementation("org.biokotlin:biokotlin") {
        version {
            branch = "develop"
        }
    }
    implementation("org.jetbrains.kotlin:kotlin-script-runtime:$kotlinVersion")
}

tasks.clean {
    delete("${rootDir}/.gradle/vcs-1")
}

task("BioKotlinExample") {
    dependsOn("build")
    println("runtime classpath: ${configurations.runtimeClasspath.get().asPath}")
    doLast {
        exec {
            commandLine("kotlinc", "-cp", configurations.runtimeClasspath.get().asPath, "-script", "src/main/kotlin/BioKotlinExample.kts")
        }
    }
}